<!DOCTYPE html>
<html>
<head>
    <title>Meteo</title>
    <meta charset="utf-8">
    <meta http-equiv="refresh" content="300">
    <link href="dist/css/bootstrap.css" rel="stylesheet">
    <link href="dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="dist/css/bootstrap-theme.css" rel="stylesheet">

    <!--    http://realfavicongenerator.net-->

<style>
    .container {
        /*width: 100%;*/
        /*max-width: 1050px;*/
        /*min-height: 1000px;*/
    }
</style>

</head>
<body>

<div class="container">

    <?php
    if (!$_GET[radar]) $_GET[radar]='UVKIzhevsk';
    $radars=array(
        'UVKIzhevsk'=>'Ижевск',
        'UVKKazan'=>'Казань',
        'UVKKirov'=>'Киров',
        'UVKSochi'=>'Сочи',
//        'UVKKrasnodar'=>'Краснодар',
//        'UVKProfsoyuz'=>'Москва',
    );

    $namem=$radars[$_GET[radar]];
    print "<h3>Метеообстановка $namem</h3>";

    foreach ( $radars as $one=>$name) {
//        print $one.$name;
        $class='primary';
        if ($one==$_GET[radar]) $class=success;
        if ($one=='UVKIzhevsk') print '<a class="btn btn-sm btn-'.$class.'" href="/">'.$name.'</a> ';
        else print '<a class="btn btn-sm btn-'.$class.'" href="/?radar='.$one.'">'.$name.'</a> ';
    }
    ?>

    <a class="btn btn-sm btn-warning" href="/flood.php">Пожелания</a>
    <!--<a class="btn btn-sm btn-success"  href="/?radar=RUDZ">Казань</a>-->
    <!--<a class="btn btn-sm btn-success"  href="/?radar=RUSI">Сочи</a>-->
    <!--<a class="btn btn-sm btn-success"  href="/?radar=RUDE">Москва</a>-->
<!--    <a class="btn btn-sm btn-success" href="/?radar=UVKIzhevsk">Ижевск</a>-->
<!--    <a class="btn btn-sm btn-success"  href="/?radar=UVKKazan">Казань</a>-->
<!--    <a class="btn btn-sm btn-success"  href="/?radar=UVKKirov">Киров</a>-->
<!--    <a class="btn btn-sm btn-success"  href="/?radar=UVKSochi">Сочи</a> -->
    <br>Если нужны еще радары - пишите мне в <a href="https://twitter.com/kilexst">twitter</a>, добавлю
    <!--<a class="btn btn-sm btn-success"  href="/?radar=RUDE">Москва</a>-->
    <br>
    <br>
    <!--<div id="picture" class=""></div>-->

<!--    <img src="./UVKKazan.gif">-->
<!--    <img src="./UVKSamara.gif">-->
<!--    <img src="./UVKSochi.gif">-->
    <?php

        print '<a href="./'.$_GET[radar].'-f.gif"><img width="800" src="./'.$_GET[radar].'.gif"></a> <img src="legend.png" alt="Легенда"> <br> <a href="./'.$_GET[radar].'-f.gif" class="btn btn-sm btn-warning">История</a>';
//    else print '<img src="./UVKIzhevsk.gif">';

    ?>

    <br>
    <br>
    <p>Анимация текущей обстановки в небе над Удмуртией (и не только). Данные отображаются за последние 5 часов. Возможно отставание, тк данные из источника всегда запоздавшие. Чтобы посмотреть более детальную и давнюю картинку - нажимайте на кнопку история или на саму карту</p>
    <!--<p>Информация нихрена не любезно взята <a href="http://meteoinfo.by/radar/?q=RUDI&t=0" target="_blank">отсюда</a> В самом деле они сами не в состоянии сделать анимацию и блокируют доступ к своим данным с подобных ресурсов. <b>Так что лучи поноса им в карму!!!!!</b></b></p>-->
    <p>Информация любезно взята <a href="http://www.meteorad.ru/data/uvkI.html" target="_blank">отсюда</a><br><b>Отдельное спасибо <a href="https://twitter.com/AlxYu">AlxYu</a> за предоставленый код автоматизации сбора гифок</b></p>
    <p>По анимации можно оценить погоду в городе <?php print $namem ?> - по движению синих и красных тучек становится понятно будет ли дождик и гроза в интересующем районе</p>
    <p>Погода в <?php print $namem ?>, осадки в <?php print $namem ?>, будет ли дождь в <?php print $namem ?>, бесплатно без смс и регистрации!</p>

    <div id="disqus_thread"></div>
    <script type="text/javascript">
        /* * * CONFIGURATION VARIABLES * * */
        var disqus_shortname = 'meteok';

        /* * * DON'T EDIT BELOW THIS LINE * * */
        (function() {
            var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
            dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
        })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>

</div>

<script src="dist/js/jquery.js"></script>
<script src="dist/js/bootstrap.js"></script>
<script src="dist/js/bootstrap.min.js"></script>
<script src="dist/js/tab.js"></script>
<script src="dist/js/dropdown.js"></script>
<script src="dist/js/button.js"></script>

<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-51757450-1', 'kilex.ru');
    ga('send', 'pageview');

</script>

</body>
</html>
